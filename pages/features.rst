..	_features:

Anticipated Features and Timelines
**********************************

The following table attempts to articulate the major features which I am working to implement for various phases of development. 

To keep track of the the latest status of the exact features being currently worked on, please see the public story board on GitLab: 
https://gitlab.com/foundrynet/foundryvtt/boards.

..  csv-table::
    :file: ../_static/features.csv
    :widths: 25, 60, 15
    :header-rows: 1
