Rolling Dice
************

At the core of most tabletop games is the requirement to roll dice. This page details the support for various dice
macros which are provided by Foundry VTT. The software has a rich underlying API

Basic Rolls
===========

For basic dice rolls, you and your players will most frequently type a `formula` in chat which is automatically parsed
and evaluated to display a dice roll result. Dice roll formulas begin with either `/roll` or `/r` for short. The
syntax for a basic roll imitates common RPGs where ``/roll ydX`` would roll an X-sided die y times. For example,
``/roll 5d6`` would roll five six-sided dice (d6). Generally:

* ``/roll {N}d{S}`` Roll ``N`` dice with ``S`` sides each and return the total result.

Dice rolls also accept modifiers after the roll which alter the behavior of the group of rolled dice. In general, roll
modifiers work by adding extra syntax on to the end of the roll, ``/roll ydX{mods}``.

Keep or Drop Results
--------------------

A desired number of high or low rolls may be kept or dropped using the following roll modifiers.

* ``kh{N}``     Keep the highest ``N`` dice out of the group that were rolled.
* ``kl{N}``     Keep the lowest ``N`` dice out of the group that were rolled.
* ``dh{N}``     Drop the highest ``N`` dice out of the group that were rolled.
* ``dl{N}``     Drop the lowest ``N`` dice out of the group that were rolled.

Re-roll Certain Results
-----------------------

Specific results may be re-rolled by specifying a re-roll target or range.

* ``r{y}``      Re-roll any dice where the result was ``y``.
* ``r>{y}``     Re-roll any dice where the result was greater than ``y``.
* ``r>={y}``    Re-roll any dice where the result was greater than or equal to ``y``.
* ``r<{y}``     Re-roll any dice where the result was less than ``y``.
* ``r<={y}``    Re-roll any dice where the result was less than or equal to ``y``.

Explode Certain Results
-----------------------

"Exploding" dice are supported where further dice of the same type are rolled until no more results of a certain value
have been rolled.

* ``x{y}``      Roll an additional dice each time a die rolls ``y``.
* ``x>{y}``     Roll an additional dice if any dice rolls greater than ``y``.
* ``x>={y}``    Roll an additional dice if any dice rolls greater than or equal to ``y``.
* ``x<{y}``     Roll an additional dice if any dice rolls less than or equal to ``y``.
* ``x<={y}``    Roll an additional dice if any dice rolls less than or equal to ``y``.

Count Successes
---------------

You can roll a set of dice and count the number of times a certain result or range of results was achieved.

* ``cs={y}``     Count the number of dice which resulted exactly in ``y``.
* ``cs>{y}``     Count the number of dice which rolled greater than ``y``.
* ``cs>={y}``    Count the number of dice which rolled greater than or equal to ``y``.
* ``cs<{y}``     Count the number of dice which rolled less than or equal to ``y``.
* ``cs<={y}``    Count the number of dice which rolled less than or equal to ``y``.

Margin of Success
-----------------

Roll a set of dice and compare the total against some target, keeping the difference as the result.

* ``ms={y}``      Subtract ``y`` from the rolled total and return the difference.
* ``ms>{y}``      Same as above.
* ``ms>={y}``     Same as above.
* ``ms<{y}``      Subtract the rolled total from `y` and return the difference.
* ``ms<={y}``     Same as above.


Parenthetical Expressions
=========================

A parenthetical expression allows you to evaluate some inner component of a roll formula before evaluating the outer
portion. This allows for options where the number, faces, or modifiers of a dice roll are themselves dynamic in some
way. Parenthetical expressions are defined by enclosing the inner expression in parentheses. Some example uses of
parenthetical expressions include:

* ``(@abilities.dex.mod)d6``        Roll a variable number of dice based on some data attribute
* ``(2d4)d8``                       Roll a variable number of dice based on another roll
* ``3d12cs>=(@attributes.power)``   Count rolls as successes if they exceed some target attribute

Dice Pools
==========

A dice pool allows you to evaluate a set of dice roll expressions and combine or choose certain results from the pool
as the final total. This allows you to keep, combine, or count results across multiple rolled formulae. Dice pools are
defined using comma separated roll terms within brackets. Some example uses of dice pools include:

* ``{3d8, 4d6}kh``                  Keep the better result of two different roll formulas
* ``{4d6, 3d8, 2d10, 1d12}cs>5``    Count the number of dice formula which exceed some target threshold
* ``{1d20, 10}kh``                  Keep the best result between a 1d20 and 10
